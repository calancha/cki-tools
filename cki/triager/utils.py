"""Triager utils."""
from functools import lru_cache
import time


def get_cache_ttl(duration=300):
    """Generate a hash to invalidate the cached call signature."""
    return round(time.time() / duration)


@lru_cache(maxsize=1000)  # big number but not too big
def get_checkout(dw_client, checkout_id, ttl_hash):
    """Retrieve and cache the checkout object."""
    del ttl_hash  # Not used, just a param to invalidate lru_cache
    return dw_client.kcidb.checkouts.get(checkout_id)


@lru_cache(maxsize=1000)  # big number but not too big
def get_build(dw_client, build_id, ttl_hash):
    """Retrieve and cache the build object."""
    del ttl_hash  # Not used, just a param to invalidate lru_cache
    return dw_client.kcidb.builds.get(build_id)
