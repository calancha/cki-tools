"""Retrigger pipelines already run."""
import argparse

from cki_lib import cki_pipeline
from cki_lib import gitlab
from cki_lib import misc
import sentry_sdk


def main() -> None:
    """Retrigger a pipline."""
    misc.sentry_init(sentry_sdk)
    parser = argparse.ArgumentParser(
        description='Trigger canary pipelines based on production pipelines')
    parser.add_argument('pipeline_url', help='Gitlab pipeline to retrigger',)
    parser.add_argument("--variables", action=misc.StoreNameValuePair, default={},
                        metavar='KEY=VALUE', help='Override trigger variables')
    args = parser.parse_args()

    gl_instance, gl_pipeline = gitlab.parse_gitlab_url(args.pipeline_url)
    with gl_instance:
        gl_retriggered_pipeline = cki_pipeline.retrigger(
            gl_instance.projects.get(gl_pipeline.project_id),
            gl_pipeline.id,
            variable_overrides=args.variables,
            is_production=misc.is_production(),
            interactive=True)
    print(f'Pipeline: {gl_retriggered_pipeline.web_url}')


if __name__ == '__main__':
    main()
