"""KCIDB Edit Entrypoint."""
import sys
from warnings import warn

from cki.kcidb import parser

if __name__ == '__main__':
    warn("Module cki.kcidb.edit will be deprecated soon, please use cki.kcidb.parser.",
         DeprecationWarning)
    parser.main(sys.argv[1:])
