"""Test CKI Cleaning Tools."""

from io import StringIO
import sys
import unittest
from unittest.mock import Mock
from unittest.mock import patch

from cki.cleaning_tools.cleaning_tools import GitLabMergeRequestManager
from cki.cleaning_tools.cleaning_tools import parse_arguments


class TestParseArguments(unittest.TestCase):
    """Test parse_arguments() function."""

    def test_parse_arguments_valid(self):
        """Test parse_arguments() function with valid input."""
        sys.argv = ['-a', '1111-11-11T11:11:11Z', '-b', '2222-22-22T22:22:22Z',
                    '-u', 'https://gitlab.com/test/test/src/kernel/test']
        args = parse_arguments(sys.argv)
        self.assertEqual(args.created_after, '1111-11-11T11:11:11Z')
        self.assertEqual(args.created_before, '2222-22-22T22:22:22Z')
        self.assertEqual(args.gitlab_url, 'https://gitlab.com/test/test/src/kernel/test')

    def test_parse_arguments_invalid(self):
        """Test parse_arguments() function with invalid input (missing required argument)."""
        sys.argv = ['-a', '1111-11-11T11:11:11Z', '-b', '2222-22-22T22:22:22Z']
        stderr = StringIO()
        sys.stderr = stderr
        with self.assertRaises(SystemExit):
            parse_arguments(sys.argv)
        self.assertIn('error: the following arguments are required: -u/--gitlab_url',
                      stderr.getvalue())


class TestGitLabMergeRequestManager(unittest.TestCase):
    """Test GitLabMergeRequestManager() class."""
    def setUp(self) -> None:
        """Set Up method for GitLabMergeRequestManager."""
        self.gitlab_url = 'https://gitlab.com/test/project/src/kernel/test'
        self.expected_project = 'test/project/src/kernel/test'
        self.mock_git_instance = Mock()

    @patch('cki.cleaning_tools.cleaning_tools.parse_gitlab_url')
    @patch('cki.cleaning_tools.cleaning_tools.GetMRsInfo')
    def test_init(self, mock_get_mrs_info, mock_parse_gitlab_url):
        """Test __init__() method for the GitLabMergeRequestManager() class."""
        mock_parse_gitlab_url.return_value = ('https://gitlab.com', self.expected_project)
        mock_get_mrs_info.return_value = Mock()
        manager = GitLabMergeRequestManager(self.gitlab_url)

        mock_parse_gitlab_url.assert_called_once_with(self.gitlab_url)
        mock_get_mrs_info.assert_called_once_with(self.expected_project)
        self.assertIsInstance(manager, GitLabMergeRequestManager)
        self.assertIsInstance(manager.git, Mock)

    @patch('cki.cleaning_tools.cleaning_tools.parse_gitlab_url')
    def test_init_invalid_url(self, mock_parse_gitlab_url):
        """Test __init__() method for the GitLabMergeRequestManager() class. Invalid url."""
        gitlab_url = 'invalid_url'
        with self.assertRaises(ValueError):
            GitLabMergeRequestManager(gitlab_url)

    @patch('cki.cleaning_tools.cleaning_tools.parse_gitlab_url')
    @patch('cki.cleaning_tools.cleaning_tools.GetMRsInfo')
    def test_get_git_merge_requests_by_period(self, mock_get_mrs_info, mock_parse_gitlab_url):
        """Test get_git_merge_requests_by_period() method."""
        mock_parse_gitlab_url.return_value = ('https://gitlab.com', self.expected_project)
        mock_get_mrs_info.return_value = Mock()
        manager = GitLabMergeRequestManager(self.gitlab_url)
        manager.git = self.mock_git_instance
        created_after = '2022-10-29T08:00:00Z'
        created_before = '2022-11-28T08:00:00Z'
        expected_result = {
            'first_mr_id': 123,
            'last_mr_id': 456,
            'total_mrs': 789
        }
        self.mock_git_instance.get_merge_requests_by_period.return_value = expected_result
        result = manager.get_git_merge_requests_by_period(created_after, created_before)
        self.mock_git_instance.get_merge_requests_by_period.assert_called_once_with(
            created_after=created_after,
            created_before=created_before)
        self.assertEqual(result, expected_result)

    @patch('cki.cleaning_tools.cleaning_tools.parse_gitlab_url')
    @patch('cki.cleaning_tools.cleaning_tools.GetMRsInfo')
    @patch('cki.cleaning_tools.s3bucketobjects')
    def test_calculate_total_bucket_size_empty(self,
                                               mock_s3bucketobjects,
                                               mock_get_mrs_info,
                                               mock_parse_gitlab_url):
        """Test calculate_total_bucket_size() method."""
        mock_parse_gitlab_url.return_value = ('https://gitlab.com', self.expected_project)
        mock_get_mrs_info.return_value = Mock()
        manager = GitLabMergeRequestManager(self.gitlab_url)
        manager.git = self.mock_git_instance
        self.mock_git_instance.get_merge_request_pipelines_before_the_last_succeed.\
            return_value = []
        manager.calculate_total_bucket_size(123)
        self.mock_git_instance.get_merge_request_pipelines_before_the_last_succeed.\
            assert_called_once_with(123)
        mock_s3bucketobjects.get_bucket_url_size.assert_not_called()

    @patch('cki.cleaning_tools.cleaning_tools.parse_gitlab_url')
    @patch('cki.cleaning_tools.cleaning_tools.GetMRsInfo')
    @patch('cki.cleaning_tools.s3bucketobjects.get_bucket_url_size')
    def test_calculate_total_bucket_size(self,
                                         mock_get_bucket_url_size,
                                         mock_get_mrs_info,
                                         mock_parse_gitlab_url):
        """Test calculate_total_bucket_size() method."""
        mock_get_mrs_info.return_value.get_merge_request_pipelines_before_the_last_succeed.\
            return_value = [{'id': 1}, {'id': 2}]
        mock_get_mrs_info.return_value.get_list_bridges_of_pipeline.return_value = [3, 4]
        mock_get_bucket_url_size.return_value = {
            'buckets': [{
                'bucket_name': 'test-bucket',
                'url': 's3://test-bucket', 'size': 1024}],
            'total_size': 1024}
        mock_parse_gitlab_url.return_value = ('https://gitlab.com', self.expected_project)
        mock_get_mrs_info.return_value = Mock()
        manager = GitLabMergeRequestManager(self.gitlab_url)
        manager.git = self.mock_git_instance
        self.mock_git_instance.get_merge_request_pipelines_before_the_last_succeed.\
            return_value = []
        manager.calculate_total_bucket_size(123)
        mock_get_mrs_info.assert_called_once_with(self.expected_project)
        self.assertEqual(None, manager.calculate_total_bucket_size(123))
