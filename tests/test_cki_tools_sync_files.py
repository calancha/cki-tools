"""Tests for the sync_files scripts."""
from unittest import TestCase
from unittest import mock

from cki.cki_tools import sync_files


class TestSyncFiles(TestCase):
    """Test cases for the sync_files script."""

    MOCK_CONFIG = {'file_list': [{'source_url': 'https://example.com/file.txt',
                                  'target_path': 'file.txt'}]
                   }

    @mock.patch('cki.cki_tools.sync_files.SESSION')
    @mock.patch('cki.cki_tools.sync_files.is_production', mock.Mock(return_value=True))
    def test_sync_files_exception(self, mock_session):
        """Returns False when there is an exception fetching the source_url."""
        bucket = mock.Mock()
        mock_session.get.side_effect = sync_files.RequestException()
        self.assertFalse(sync_files.sync_file(bucket, 'source_url', 'target_path'))
        bucket.client.put_object.assert_not_called()

    @mock.patch('cki.cki_tools.sync_files.SESSION')
    @mock.patch('cki.cki_tools.sync_files.is_production', mock.Mock(return_value=True))
    def test_sync_files_puts_object(self, mock_session):
        """Returns True when the data is fetched and put to the bucket."""
        bucket = mock.Mock()
        bucket.spec = mock.Mock(prefix='')
        mock_response = mock.Mock(text='cool file',
                                  headers={'content-length': 123,
                                           'content-type': 'text/plain; utf-8'}
                                  )
        mock_session.get.return_value = mock_response
        self.assertTrue(sync_files.sync_file(bucket, 'source_url', 'target_path'))
        bucket.client.put_object.assert_called_once()

    @mock.patch('cki.cki_tools.sync_files.SYNC_CONFIG', {})
    def test_main_no_config(self):
        """Returns -1 when there is no configuration data to process."""
        with mock.patch('sys.exit') as mock_exit:
            sync_files.main()
            mock_exit.assert_called_once_with(-1)

    @mock.patch('cki.cki_tools.sync_files.sync_file')
    @mock.patch('cki.cki_tools.sync_files.BUCKET_CONFIG_NAME', 'BUCKET')
    @mock.patch.dict('os.environ', {'BUCKET': 'foo'})
    @mock.patch('cki.cki_tools.sync_files.S3Bucket', mock.Mock(return_value=True))
    def test_main_with_config(self, mock_sync_file):
        """Returns 0 when everything goes well, otherwise -1."""
        # sync_file returns True, sys.exit(0)
        mock_sync_file.return_value = True
        with mock.patch('sys.exit') as mock_exit:
            with mock.patch('cki.cki_tools.sync_files.SYNC_CONFIG', self.MOCK_CONFIG):
                sync_files.main()
                mock_exit.assert_called_once_with(0)
        # sync_file returns False, sys.exit(1)
        mock_sync_file.return_value = False
        with mock.patch('sys.exit') as mock_exit:
            with mock.patch('cki.cki_tools.sync_files.SYNC_CONFIG', self.MOCK_CONFIG):
                sync_files.main()
                mock_exit.assert_called_once_with(1)
