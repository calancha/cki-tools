"""Utility functions for testcases."""
import base64
import gzip

from prometheus_client.registry import REGISTRY


def tear_down_registry():
    # pylint: disable=protected-access
    """Unregister collectors in prometheus registry."""
    collectors = list(REGISTRY._collector_to_names.keys())
    for collector in collectors:
        REGISTRY.unregister(collector)


def b64decode_and_decompress(data: str) -> str:
    """Decode and uncompress base64-encoded and gzip-compressed data."""
    return gzip.decompress(base64.b64decode(data)).decode('utf-8')
