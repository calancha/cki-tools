"""Test triager."""
import unittest
from unittest.mock import patch

from datawarehouse import Datawarehouse
import responses

from cki.triager.checkers import CheckoutFailureChecker
from cki.triager.checkers import FailureChecker
from cki.triager.checkers import LOGGER as checkers_logger
from cki.triager.regexes import RegexChecker
from cki.triager.triager import DWObject

MOCK_REGEXES = [
    {
        "id": 2,
        "issue": {
            "id": 88,
            "kind": {
                "id": 1,
                "description": "Kernel bug",
                "tag": "Kernel Bug"
            },
            "description": "Bug description",
            "ticket_url": "https://bug.link",
            "resolved": False,
            "generic": False
        },
        "text_match": "bnx2x .* Direct firmware load for",
        "file_name_match": None,
        "test_name_match": None
    },
    {
        "id": 15,
        "issue": {
            "id": 126,
            "kind": {
                "id": 1,
                "description": "Kernel bug",
                "tag": "Kernel Bug"
            },
            "description": "BUG: clone.*failing after kernel commit",
            "ticket_url": "http://url.com",
            "resolved": False,
            "generic": False
        },
        "text_match": "tag=kcmp03.*FAIL: clone",
        "file_name_match": "syscalls.fail.log",
        "test_name_match": "LTP"
    }
]


class FailureCheckerTest(unittest.TestCase):
    """Test FailureChecker."""

    def setUp(self):
        """Set up tests."""
        FailureChecker._download.cache_clear()  # pylint: disable=protected-access
        RegexChecker._download_lookups.cache_clear()  # pylint: disable=protected-access

    @responses.activate
    @patch('cki.triager.regexes.utils.get_build', lambda *args: None)
    @patch('cki.triager.regexes.dw_client', Datawarehouse('http://datawarehouse'))
    def test_check_logs_with_regex(self):
        """Test check_logs_with_regex."""
        responses.add(responses.HEAD, url='https://logs/foobar', content_type='not/plain-text')
        responses.add(responses.HEAD, url='https://logs/console.log', content_type='not/plain-text')
        responses.add(responses.GET, url='https://logs/console.log',
                      body=(b'[   41.451946] bnx2x 0045:01:00.0: Direct firmware load for '
                            b'bfq_bfqq_move '))
        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/-/regex',
                      json={'results': MOCK_REGEXES})
        test = DWObject('test', {
            'build_id': 'redhat:952371',
            'id': 'redhat:113990600',
            'path': 'boot',
            'comment': 'Boot test',
            'duration': 0,
            'output_files': [
                {'url': 'https://logs/foobar', 'name': '8704397_aarch64_2_foobar'},
                {'url': 'https://logs/console.log',
                 'name': '8704397_aarch64_2_systemd_journal.log'},
            ]
        })

        issues = FailureChecker(test).check_logs_with_regex()
        self.assertEqual(
            [{'name': 'Bug description', 'id': 88, 'regex_id': 2}],
            issues)

        # Logs have no info.
        responses.replace(responses.GET, url='https://logs/console.log',
                          body=b'2019-11-17 07:15:09,105   ')
        issues = FailureChecker(test).check_logs_with_regex()
        self.assertEqual([], issues)

    @responses.activate
    @patch('cki.triager.regexes.utils.get_build', lambda *args: None)
    @patch('cki.triager.regexes.dw_client', Datawarehouse('http://datawarehouse'))
    def test_check_logs_with_regex_logfile_redirects(self):
        """Test check_logs_with_regex."""
        redirecting_url = "https://logs/foobar"
        file_url = "https://real-logs/foobar"

        head_1 = responses.add(responses.HEAD, redirecting_url, status=301,
                               headers={"Location": file_url})
        responses.add(responses.GET, redirecting_url, status=301,
                      headers={"Location": file_url})

        head_2 = responses.add(responses.HEAD, url=file_url, content_type='text/plain')
        responses.add(responses.GET, url=file_url,
                      body=(b'[   41.451946] bnx2x 0045:01:00.0: Direct firmware load for '
                            b'bfq_bfqq_move '))
        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/-/regex',
                      json={'results': MOCK_REGEXES})
        test = DWObject('test', {
            'build_id': 'redhat:952371',
            'id': 'redhat:113990600',
            'path': 'boot',
            'comment': 'Boot test',
            'duration': 0,
            'output_files': [{'url': redirecting_url, 'name': 'foobar'}]
        })

        with self.subTest("Log content-type is valid ()= text/plain)"):
            issues = FailureChecker(test).check_logs_with_regex()
            self.assertEqual(head_1.call_count, 1)
            self.assertEqual(head_2.call_count, 1)
            self.assertEqual(
                [{'name': 'Bug description', 'id': 88, 'regex_id': 2}],
                issues)

        bad_content_type = 'not/plain'
        head_2 = responses.replace(responses.HEAD, url=file_url, content_type=bad_content_type)
        FailureChecker._download.cache_clear()  # pylint: disable=protected-access

        with self.subTest("Log content-type is invalid (!= text/plain)"):
            with patch.object(checkers_logger, "debug") as mocked_debug_logger:
                issues = FailureChecker(test).check_logs_with_regex()

            self.assertEqual(head_2.call_count, 1)
            self.assertEqual([], issues)
            mocked_debug_logger.assert_called_with(
                "Couldn't read file %r with content_type=%r", redirecting_url, bad_content_type)

    @responses.activate
    @patch('cki.triager.regexes.utils.get_build', lambda *args: None)
    @patch('cki.triager.regexes.dw_client', Datawarehouse('http://datawarehouse'))
    def test_check_logs_with_regex_multiple_matches(self):
        """Test check_logs_with_regex. More than one regex matches."""
        responses.add(responses.HEAD, url='https://logs/console.log')
        responses.add(responses.HEAD, url='https://logs/syscalls.fail.log')
        responses.add(responses.GET, url='https://logs/console.log',
                      body=(b'[   41.451946] bnx2x 0045:01:00.0: Direct firmware load for '
                            b'bfq_bfqq_move '))
        responses.add(responses.GET, url='https://logs/syscalls.fail.log',
                      body=(b'[   41.451946] tag=kcmp03 error error FAIL: clone'))

        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/-/regex',
                      json={'results': MOCK_REGEXES})
        test = DWObject('test', {
            'build_id': 'redhat:952371',
            'id': 'redhat:113990600',
            'path': 'ltp',
            'comment': 'LTP',
            'duration': 0,
            'output_files': [
                {'url': 'https://logs/console.log',
                 'name': '8704397_aarch64_2_systemd_journal.log'},
                {'url': 'https://logs/syscalls.fail.log',
                 'name': '8704397_aarch64_2_syscalls.fail.log'},
            ]
        })

        issues = FailureChecker(test).check_logs_with_regex()
        self.assertEqual(
            [
                {'id': 88, 'name': 'Bug description', 'regex_id': 2},
                {'id': 126, 'name': 'BUG: clone.*failing after kernel commit', 'regex_id': 15}
            ],
            issues
        )

    @patch('cki.triager.checkers.FailureChecker.check_logs_with_regex')
    def test_check_check_all(self, check1):
        """Test check_all call."""
        issues = FailureChecker(None)
        issues.check_all()

        self.assertTrue(check1.called)

    @patch('cki.triager.checkers.FailureChecker.check_all')
    def test_check_check(self, check_all):
        """Test check call."""
        FailureChecker.check(None)

        self.assertTrue(check_all.called)

    @staticmethod
    @responses.activate
    @patch('cki.triager.regexes.dw_client', Datawarehouse('http://datawarehouse'))
    def test_check_logs_with_regex_lazy():
        """Test check_logs_with_regex doesnt download file if not necessary."""
        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/-/regex',
                      json={'results': [MOCK_REGEXES[1]]})  # This regex has test constraints
        test = DWObject('test', {
            'build_id': 'redhat:952371',
            'id': 'redhat:113990600',
            'path': 'boot',
            'comment': 'Boot test',
            'duration': 0,
            'output_files': [
                {'url': 'https://logs/foobar', 'name': '8704397_aarch64_2_foobar'},
            ]
        })

        # check_logs_with_regex does not fail to find the mocked URL
        FailureChecker(test).check_logs_with_regex()


class CheckoutFailureCheckerTest(unittest.TestCase):
    """Test CheckoutFailureChecker."""

    def test_logfiles(self):
        """Test logfiles property."""
        checkout = DWObject('checkout', {
            'id': 'redhat:123',
            'log_url': 'https://log/url'
        })

        self.assertEqual(
            CheckoutFailureChecker(checkout).logfiles,
            [{'name': 'merge.log', 'url': 'https://log/url'}]
        )

    def test_logfiles_empty(self):
        """Test logfiles property."""
        checkout = DWObject('checkout', {
            'id': 'redhat:123',
        })

        self.assertEqual(
            CheckoutFailureChecker(checkout).logfiles,
            []
        )
