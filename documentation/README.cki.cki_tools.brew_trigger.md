# `cki.cki_tools.brew_trigger`

```shell
python3 -m cki.cki_tools.brew_trigger [-c CONFIG] [options] task_id
```

Trigger a CKI pipeline for a kernel from Brew/Koji. The same configuration
files as for the [brew-trigger pipeline-trigger module] can be used. If
`CKI_DEPLOYMENT_ENVIRONMENT` variable is not present or not set to
`production`, retriggered pipelines are created instead of production.

[brew-trigger pipeline-trigger module]: https://gitlab.com/cki-project/pipeline-trigger/-/blob/main/triggers/brew_trigger.py
