# `cki.cki_tools.gitlab_sso_login`

Login to GitLab via a SAML provider, and optionally run a smoke test.

```shell
Usage: python3 -m cki.cki_tools.gitlab_sso_login
```

For logging into Kerberos, either a keytab or a password is needed.

## Environment variables

| Name                     | Required | Description                                                              |
|--------------------------|----------|--------------------------------------------------------------------------|
| `KRB_KEYTAB`             | no       | Name of the environment variable with the base64-encoded keytab contents |
| `KRB_PRINCIPAL`          | no       | Kerberos principal to use                                                |
| `KRB_PASSWORD`           | no       | Name of the environment variable with the Kerberos password              |
| `SSO_SAML_AUTH_URL`      | yes      | Identity provider single sign-on URL                                     |
| `GITLAB_SAML_CB_URL`     | yes      | Assertion consumer service URL                                           |
| `SMOKE_TEST_PROJECT_URL` | no       | GitLab project URL that can only be accessed when logged in              |
