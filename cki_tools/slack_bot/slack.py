"""Slack message sending."""
import os

from cki_lib.logger import get_logger
from cki_lib.session import get_session

LOGGER = get_logger(__name__)
SESSION = get_session(__name__)


def send_message(message: str) -> None:
    """Send a message to the Slack webhook."""
    slack_webhook_url = os.environ.get('CKI_SLACK_BOT_WEBHOOK')
    webhook_payload = {
        "text": message,
        "unfurl_links": False,
        "unfurl_media": False,
    }
    LOGGER.info('Sending message: %s', message)
    SESSION.post(slack_webhook_url, json=webhook_payload)
